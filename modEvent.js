	var MODEvent = {
		acr: function (result) {
			document.getElementById("systemAcr").innerHTML = result.status;
			if(result.status == "ok") {
				document.getElementById("systemChannel").innerHTML = result.channel;
			}

			showCallback("MODEvent.acr", result);
		},
		keepon: function (result) {
			document.getElementById("systemKeepon").innerHTML = result.status;
			showCallback("MODEvent.keepon", result);
		},
		lockback: function (result) {
			document.getElementById("systemLockback").innerHTML = result.status;
			showCallback("MODEvent.lockback", result);
		},
		setUuid: function (id) {
			document.getElementById("systemUuid").innerHTML = id;
			showCallback("MODEvent.setUuid", id);
		},
		setVersion: function (data) {
			document.getElementById("systemVersion").innerHTML = data.version;
			document.getElementById("systemPlatform").innerHTML = data.platform;
			showCallback("MODEvent.setVersion", data);
		},
		reminder: function (result) {
			document.getElementById("systemReminder").innerHTML = result.status;
			showCallback("MODEvent.setUuid", result);
		},
		shake: function (result) {
			document.getElementById("systemShake").innerHTML = result.status;
			showCallback("MODEvent.shake", result);
			if(result.status == "shaking") {
				window.location.href = 'tevent://shake.stop';
			}
		},
		fb: {
			token: "",
			friends: [],
			share: function (result) {
				document.getElementById("facebookShare").innerHTML = result.status;
				showCallback("MODEvent.fb.share", result);
			},
			request: function (result) {
				document.getElementById("facebookRequest").innerHTML = result.status;
				showCallback("MODEvent.fb.request", result);
			},
			setUser: function (user) {
				this.token = user.token;
				document.getElementById("facebookId").innerHTML = user.id;
				document.getElementById("facebookToken").innerHTML = user.token;
				document.getElementById("facebookEmail").innerHTML = user.email;
				document.getElementById("facebookName").innerHTML = user.name;
				document.getElementById("facebookEquipmentId").innerHTML = user.equipmentId;
				document.getElementById("facebookImgUrl").innerHTML = user.imgUrl;
				showCallback("MODEvent.fb.setUser", user);
			},
			setFriends: function (list) {
				var friendList = [];
				this.friends = this.friends.concat(list);
				var friendList = "";
				for (var i = this.friends.length - 1; i >= 0; i--) {
					friendList += this.friends[i].name + " ";
				};
				document.getElementById("facebookFriends").innerHTML = friendList;
				showCallback("MODEvent.fb.setFriends", list);
			},
			getToken: function () {
				return this.token;
			}
		}
	};

	function linkToken() {
		window.location.href = "https://graph.facebook.com/me?access_token=" + modEvent.fb.getToken();
	}

	function showBridgeToast(toast) {
		Bridge.showToast(toast);
	}

	function setReminder() {
		var remindTime = moment().add(180, 'seconds').toDate();
		window.location.href = "tevent://reminder/%E6%99%82%E9%96%93%E5%88%B0%E5%9B%89!!!/" + remindTime.valueOf();
	}

	function showCallback(method, param) {
		document.getElementById("callbackMethod").innerHTML = method;
		document.getElementById("callbackParam").innerHTML = JSON.stringify(param);
	}

	function clearAll() {
		var callbackForms = document.getElementsByClassName("callbackForm");
		for (var i = callbackForms.length - 1; i >= 0; i--) {
			callbackForms[i].innerHTML = "";
		};
	}
